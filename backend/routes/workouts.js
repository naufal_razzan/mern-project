const express = require('express')
const {
    createWorkout,
    getList,
    getOne,
    deleteWorkout,
    updateWorkout
} = require('../controllers/workout.controller')
const requireAuth = require('../middleware/requireAuth')

const router = express.Router()

router.use(requireAuth)

// GET all workouts
router.get('/', getList)

// GET a single workout
router.get('/:id', getOne)

//POST a new workout
router.post('/', createWorkout)

//DELETE a workout
router.delete('/:id', deleteWorkout)

// UPDATE a workout
router.patch('/:id', updateWorkout)

module.exports = router