const Workout = require('../models/workout.model')
const mongoose = require('mongoose')

// get all workout list
const getList = async (req, res) => {
    const user_id = req.user._id
    
    try {
        const workouts = await Workout.find({ user_id }).sort({createdAt: -1})
        res.status(200).json(workouts)
    } catch (err) {
        res.status(500).json({error: err.message})
        console.log("Error to get all lists", err)
    }
}

// get a single workout
const getOne = async (req, res) => {
    const { id } = req.params

    try {
        if(!mongoose.Types.ObjectId.isValid(id)){
            return res.status(400).json({error: 'Not a valid parameters'})
        }
        const workout = await Workout.findById(id)
        if(!workout){
            return res.status(404).json({error: 'No such workout exists'})
        }
        res.status(200).json(workout)
    } catch (err) {
        res.status(500).json({error: err.message})
        console.log("Error to get a single list", err)
    }
}

// create new workout
const createWorkout = async (req, res) => {
    const { title, load, reps } = req.body

    let emptyFields = []

    if(!title){
        emptyFields.push('title')
    }
    if(!load){
        emptyFields.push('load')
    }
    if(!reps){
        emptyFields.push('reps')
    }
    if(emptyFields.length > 0){
        return res.status(400).json({ error: 'Please fill in all the fields', emptyFields })
    }

    try {
        const user_id = req.user._id
        const workout = await Workout.create({title, load, reps, user_id})
        res.status(200).json(workout)
    }catch (err){
        res.status(500).json({error: err.message})
        console.log("Error to add new workout list", err)
    }

    // res.json({mssg: 'POST a new workout'})
    console.log("Client added new POST request")
}

// delete a workout
const deleteWorkout = async (req, res) => {
    const { id } = req.params

    try {
        if(!mongoose.Types.ObjectId.isValid(id)){
            return res.status(400).json({error: 'Not a valid parameters'})
        }
        const workout = await Workout.findOneAndDelete({_id: id})
        if(!workout){
            return res.status(404).json({error: 'No such workout exists'})
        }
        res.status(200).json({workout})
    } catch (err) {
        res.status(500).json({error: err.message})
        console.log("Error to delete a workout list", err)
    }
    console.log("Client delete a list using DELETE request")
}

//update a workout
const updateWorkout = async (req, res) => {
    const { id } = req.params

    try {
        if(!mongoose.Types.ObjectId.isValid(id)){
            return res.status(400).json({error: 'Not a valid parameters'})
        }
        const workout = await Workout.findOneAndUpdate({_id: id}, {
            ...req.body
        })
        if(!workout){
            return res.status(404).json({error: 'No such workout exists'})
        }
        res.status(200).json(workout)
    } catch (err) {
        res.status(500).json({error: err.message})
        console.log("Error to update a workout list", err)
    }
}

module.exports = {
    getList,
    getOne,
    createWorkout,
    deleteWorkout,
    updateWorkout
}